const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .copy('node_modules/datatables/media/css/jquery.dataTables.css', 'resources/assets/css/dataTables.css')
    .copy('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css', 'resources/assets/css/bootstrap-datepicker.css')
    .copyDirectory('node_modules/datatables/media/images', 'public/images');


mix
    .js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .styles([
        'resources/assets/css/dataTables.css',
        'resources/assets/css/bootstrap-datepicker.css'
    ], 'public/css/plugins.css');
