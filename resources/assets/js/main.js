$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $(document).on('submit', '.destroyItem', function(e) {
        var r = confirm("Are you sure?");
        if (r == false) {
            return false;
        }
    })
});