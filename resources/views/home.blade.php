@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Navigation</div>

                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li role="presentation" @if(\Route::currentRouteName() == 'dashboard') class="active" @endif><a href="{{ route('dashboard') }}">DashBoard</a></li>
                        <li role="presentation" @if(preg_match('/^tasks./', \Route::currentRouteName())) class="active" @endif><a href="{{ route('tasks.index') }}">Tasks</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">@section('page_header') Dashboard @show</div>

                <div class="panel-body">

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @section('page_body')
                        You are logged in!
                    @show
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
