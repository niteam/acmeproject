@extends('home')

@section('page_header') Tasks List @endsection

@section('page_body')
    <div class="action-buttons pull-right">
        <a href="{{route('tasks.create')}}" class="btn btn-primary">Add</a>
    </div>

    <table class="table table-bordered" id="tasks-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Done</th>
            <th>Date</th>
            <th>Actions</th>
        </tr>
        </thead>
    </table>

@endsection

@push('scripts_footer')
    <script>
        $(document).ready(function(){
            $(function() {
                $('#tasks-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{!! route('tasks.datatable') !!}',
                    "order": [],
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'description', name: 'description' },
                        { data: 'is_done', name: 'is_done' },
                        { data: 'date', name: 'date' },
                        { data: 'actions', name: 'actions' }
                    ]
                });
            });
        });
    </script>
@endpush