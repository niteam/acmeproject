@extends('home')

@section('page_header') Show task @endsection

@section('page_body')

    <div class="task-details">
        <ul>
            <li><strong>ID:</strong> {{$task->getId()}}</li>
            <li><strong>Name:</strong> {{$task->getName()}}</li>
            <li><strong>Description:</strong> {{$task->getDescription()}}</li>
            <li><strong>Done:</strong> {{$task->showStatus()}}</li>
            <li><strong>Date:</strong> {{$task->getDate()}}</li>
        </ul>
        <a href="{{route('tasks.index')}}" class="btn btn-danger">Back</a>
    </div>

@endsection