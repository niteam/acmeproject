@if(isset($errors) && $errors->count() > 0)
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif
<form action="{{$action_url}}" method="POST">
    {{ csrf_field() }}
    @if(isset($task))
        {{ method_field('PUT') }}
    @endif
    <div class="form-group">
        <label for="inputName">Name <span class="required">*</span></label>
        <input type="text" class="form-control" value="@if(isset($task)) {{ $task->getName() }} @endif" name="name" id="inputName" placeholder="Name">
    </div>
    <div class="form-group">
        <label for="inputDescription">Description <span class="required">*</span></label>
        <textarea class="form-control" name="description" id="inputDescription" placeholder="Description">@if(isset($task)){{ $task->getDescription() }}@endif</textarea>
    </div>
    <div class="form-group">
        <label for="inputDate">Date</label>
        <input type="text" class="form-control datepicker" value="@if(isset($task)){{ $task->getDate() }}@else{{ date('Y-m-d') }}@endif" id="inputDate" placeholder="Date" name="date">
    </div>
    <div class="checkbox">
        <label>
            <input name="is_done" @if(isset($task) && $task->getIsDone() === true) checked @endif type="checkbox"> Is done?
        </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="{{route('tasks.index')}}" class="btn btn-danger">Cancel</a>
</form>