@extends('home')

@section('page_header') Add task @endsection

@section('page_body')

    <div class="form-wrapper clearboth">
        @include('task.parts.form', ['action_url' => route('tasks.store')])
    </div>


@endsection