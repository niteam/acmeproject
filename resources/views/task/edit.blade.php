@extends('home')

@section('page_header') Edit task @endsection

@section('page_body')

    <div class="form-wrapper clearboth">
        @include('task.parts.form', ['action_url' => route('tasks.update', ['task' => $task->getId()])])
    </div>


@endsection