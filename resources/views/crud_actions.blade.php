<div class="crud-actions">
    <a href="{{route('tasks.show', ['task' => $id])}}" class="btn btn-warning" title="show">
        <span class="glyphicon glyphicon-zoom-in"></span>
    </a>
    <a href="{{route('tasks.edit', ['task' => $id])}}" class="btn btn-primary" title="edit">
        <span class="glyphicon glyphicon-edit"></span>
    </a>
    <form class="destroyItem" action="{{route('tasks.destroy', ['task' => $id])}}" method="POST">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button type="submit"class="btn btn-danger" title="destroy">
            <span class="glyphicon glyphicon-trash"></span>
        </button>
    </form>
    @if(!$is_done)
    <a href="{{route('tasks.done', ['task' => $id])}}" class="btn btn-success" title="done">
        <span class="glyphicon glyphicon glyphicon-ok"></span>
    </a>
    @endif
</div>