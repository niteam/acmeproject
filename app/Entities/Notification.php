<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

class Notification extends \LaravelDoctrine\ORM\Notifications\Notification
{
    /**
     * @ORM\ManyToOne(targetEntity="App\User")
     */
    protected $user;
}