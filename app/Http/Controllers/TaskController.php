<?php

namespace App\Http\Controllers;

use App\Entities\Task;
use App\Http\Requests\DestroyTask;
use App\Http\Requests\StoreTask;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('task.index');
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable() {

        $user = \Auth::user();

        $tasks = $user->getTasks();

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $tasks = json_decode($serializer->serialize($tasks, 'json'), true);

        $datatable = Datatables::of($tasks)
            ->editColumn('date', function($row) {
                $date = new \DateTime($row['date']);
                return $date->format('Y-m-d');
            })
            ->editColumn('is_done', function($row){
                return $row['is_done'] ? 'Yes' : 'No';
            })
            ->addColumn('actions', 'crud_actions')
            ->rawColumns(['actions'])
            ->make(true);

        return $datatable;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreTask  $request
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTask $request, EntityManagerInterface $em)
    {
        $task = new Task(
            $request->get('name'),
            $request->get('description'),
            new \DateTime($request->get('date'))
        );

        if( !is_null($request->get('is_done')) ) {
            $task->setIsDone(true);
        } else {
            $task->setIsDone(false);
        }

        $task->setUser(\Auth::user());

        $em->persist($task);
        $em->flush();

        return redirect( route('tasks.index') )->with('success', 'Task created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @return \Illuminate\Http\Response
     */
    public function show($id, EntityManagerInterface $em)
    {
        $task = $em->getRepository(Task::class)->find($id);
        if(is_null($task))
            abort(404);

        return view('task.show', ['task' => $task]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @return \Illuminate\Http\Response
     */
    public function edit($id, EntityManagerInterface $em)
    {
        $task = $em->getRepository(Task::class)->find($id);
        if(is_null($task))
            abort(404);

        return view('task.edit', ['task' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreTask  $request
     * @param  int  $id
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTask $request, $id, EntityManagerInterface $em)
    {
        $task = $em->getRepository(Task::class)->find($id);

        $task->setName( $request->get('name') );
        $task->setDescription( $request->get('description') );
        $task->setDate( new \DateTime($request->get('date')) );

        if( !is_null($request->get('is_done')) ) {
            $task->setIsDone(true);
        } else {
            $task->setIsDone(false);
        }
        $em->persist($task);
        $em->flush();

        return redirect( route('tasks.edit', ['task' => $task->getId()]) )->with('success', 'Task updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, EntityManagerInterface $em)
    {
        $task = $em->getRepository(Task::class)->find($id);
        if(is_null($task))
            abort(404);

        $em->remove($task);
        $em->flush();

        return redirect( route('tasks.index') )->with('success', 'Task deleted!');
    }

    /**
     * Set task status as Done
     *
     * @param  int  $id
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @return \Illuminate\Http\Response
     */
    public function setAsDone($id, EntityManagerInterface $em)
    {
        $task = $em->getRepository(Task::class)->find($id);
        if(is_null($task))
            abort(404);

        $task->setIsDone(true);
        $em->persist($task);
        $em->flush();

        return redirect( route('tasks.index') )->with('success', 'Status changed as done for '.$task->getName());
    }
}
