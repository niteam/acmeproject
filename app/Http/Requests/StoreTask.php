<?php

namespace App\Http\Requests;

use App\Entities\Task;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'name' => [
                'required',
                'string',
                'min:3',
            ],
            'description' => [
                'required',
                'min:3',
                'max:500'
            ],
            'date' => [
                'date_format:"Y-m-d"'
            ]
        ];

        if(!is_null($this->task)) {
            $taskId = $this->task;
            $rules['name'][] = Rule::unique(Task::class)->ignore( $taskId );
            $rules['name'][] =  Rule::exists(Task::class)->where('id', $taskId);
        }

        return $rules;
    }
}
