<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $this->browse(function (Browser $browser) {

            $faker = Factory::create();

            $browser->visit('/register')
                    ->assertSee('Register')
                    ->value('[name="name"]', $faker->name)
                    ->value('[name="email"]', $faker->email)
                    ->value('[name="password"]', 'testing')
                    ->value('[name="password_confirmation"]', 'testing')
                    ->click('#register_button')
                    ->pause(2000);
        });
    }
}
