<?php

namespace Tests\Browser;

use App\User;
use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AddTaskTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAddTaskExample()
    {
        $user = factory(User::class)->create([
            'email' => 'taylor@laravel.com',
        ]);

        $this->browse(function (Browser $browser) use ($user) {

            $faker = Factory::create();

            $taskName= $faker->text(20);
            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertPathIs('/home')
                    ->visit('tasks/create')
                    ->type('name', $taskName)
                    ->type('description', $faker->text(250))
                    ->type('date', $faker->date('Y-m-d'))
                    ->press('Submit')
                    ->visit('/tasks')
                    ->pause(2000)
                    ->assertSee($taskName)
                    ->pause(2000);
        });
    }
}
